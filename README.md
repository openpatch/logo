A little website to find a right variant for the logo of OpenPatch.

When you have found a good combination, please be so kind and share it with us.
Just mark the logo and send us an e-mail
[logo@openpatch.org](mailto:logo@openpatch.org) with the corresponding json
snippet. If you have another logo design in mind, which is not configurable via
the website, we would also like to see your awesome idea.  Just send a photo, a
scribble or just an idea in plain text to
[logo@openpatch.org](mailto:logo@openpatch.org).

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
