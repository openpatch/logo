import React, { Component } from 'react';
import Logo from './Logo';
import ColorScheme from 'color-scheme';

/*eslint-disable no-extend-native*/
Array.prototype.pairs = function(func) {
  for (var i = 0; i < this.length - 1; i++) {
    for (var j = i; j < this.length - 1; j++) {
      func([this[i], this[j + 1]]);
    }
  }
};

class App extends Component {
  state = {
    colors: [],
    baseColor: 'FF0000',
    size: 200,
    primaryColor: '006f95',
    secondaryColor: '98ff98',
    markedStyles: [],
    style: {
      inverted: false,
      textVariant: '1',
      circle: false
    }
  };

  componentDidMount() {
    this.generateRandomColorScheme();
    const markedStyles =
      JSON.parse(localStorage.getItem('markedStyles')) ||
      this.state.markedStyles;
    const primaryColor =
      localStorage.getItem('primaryColor') || this.state.primaryColor;
    const secondaryColor =
      localStorage.getItem('secondaryColor') || this.state.secondaryColor;
    const baseColor = localStorage.getItem('baseColor') || this.state.baseColor;
    const size = localStorage.getItem('size') || this.state.size;
    const style = JSON.parse(localStorage.getItem('style')) || this.state.style;

    this.setState({
      markedStyles,
      primaryColor,
      secondaryColor,
      baseColor,
      size,
      style
    });
  }

  generateRandomColorScheme = () => {
    const randomColor = '000000'.replace(/0/g, function() {
      return (~~(Math.random() * 16)).toString(16);
    });
    this.setState(
      {
        baseColor: randomColor
      },
      () => this.generateColorScheme()
    );
  };

  generateColorScheme = () => {
    const scm = new ColorScheme();
    scm
      .from_hex(this.state.baseColor)
      .scheme('tetrade')
      .variation('pastel')
      .web_safe(true);

    const colors = scm.colors();
    this.setState({
      colors
    });
  };

  onChangeBaseColor = e => {
    this.setState({
      baseColor: e.currentTarget.value
    });
    localStorage.setItem('baseColor', e.currentTarget.value);
  };
  increaseSize = () => {
    this.setState({
      size: this.state.size + 10
    });
  };
  decreaseSize = () => {
    const newSize = this.state.size - 10;
    this.setState({
      size: newSize > 0 ? newSize : this.state.size
    });
  };
  normalizeSize = () => {
    this.setState({
      size: 200
    });
  };
  onChangePrimaryColor = e => {
    this.setState({
      primaryColor: e.currentTarget.value
    });
    localStorage.setItem('primaryColor', e.currentTarget.value);
  };
  onChangeSecondaryColor = e => {
    this.setState({
      secondaryColor: e.currentTarget.value
    });
    localStorage.setItem('secondaryColor', e.currentTarget.value);
  };
  markPair = pair => {
    const styles = {
      primaryColor: pair[0],
      secondaryColor: pair[1],
      ...this.state.style
    };
    const newMarkedStyles = [...this.state.markedStyles, styles];
    this.setState({
      markedStyles: newMarkedStyles
    });
    localStorage.setItem('markedStyles', JSON.stringify(newMarkedStyles));
  };
  unmarkPair = id => {
    this.state.markedStyles.splice(id, 1);
    const newMarkedStyles = [...this.state.markedStyles];
    this.setState({
      markedStyles: newMarkedStyles
    });
    localStorage.setItem('markedStyles', JSON.stringify(newMarkedStyles));
  };

  onChangeInverted = () => {
    this.setState(
      {
        style: {
          ...this.state.style,
          inverted: !this.state.style.inverted
        }
      },
      () => this.saveStyle()
    );
  };

  onChangeTextVariant = e => {
    this.setState(
      {
        style: {
          ...this.state.style,
          textVariant: e.currentTarget.value
        }
      },
      () => this.saveStyle()
    );
  };

  onChangeCircle = () => {
    this.setState(
      {
        style: {
          ...this.state.style,
          circle: !this.state.style.circle
        }
      },
      () => this.saveStyle()
    );
  };

  saveStyle = () => {
    localStorage.setItem('style', JSON.stringify(this.state.style));
  };

  render() {
    const logos = [];
    let i = 0;
    this.state.colors.pairs(pair =>
      logos.push(
        <div style={{ margin: 8, display: 'inline-block' }} key={i++}>
          <div
            style={{
              cursor: 'pointer',

              mozUserSelect: 'none',
              webkitUserSelect: 'none',
              msUserSelect: 'none'
            }}
            onClick={() => this.markPair(pair)}
          >
            <Logo
              height={this.state.size}
              primary={'#' + pair[0]}
              secondary={'#' + pair[1]}
              {...this.state.style}
            />
          </div>
          <div>primary: #{pair[0]}</div>
          <div>secondary: #{pair[1]}</div>
        </div>
      )
    );
    const markedLogos = [];

    this.state.markedStyles.forEach(
      ({ primaryColor, secondaryColor, ...style }, k) =>
        markedLogos.push(
          <div style={{ margin: 8, display: 'inline-block' }} key={k}>
            <div
              style={{
                cursor: 'pointer',
                mozUserSelect: 'none',
                webkitUserSelect: 'none',
                msUserSelect: 'none'
              }}
              onClick={() => this.unmarkPair(k)}
            >
              <Logo
                height={this.state.size}
                primary={'#' + primaryColor}
                secondary={'#' + secondaryColor}
                {...style}
              />
            </div>
            <div>
              <pre style={{ textAlign: 'left' }}>
                {JSON.stringify(
                  { primaryColor, secondaryColor, style },
                  null,
                  2
                )}
              </pre>
            </div>
          </div>
        )
    );

    return (
      <div style={{ textAlign: 'center' }}>
        <label for="inverted">Inverted</label>
        <input
          type="checkbox"
          name="inverted"
          checked={this.state.style.inverted}
          onChange={this.onChangeInverted}
        />
        <label for="circle">Circle</label>
        <input
          type="checkbox"
          name="circle"
          checked={this.state.style.circle}
          onChange={this.onChangeCircle}
        />
        <select
          onChange={this.onChangeTextVariant}
          value={this.state.style.textVariant}
        >
          <option value="1">Variant 1</option>
          <option value="2">Variant 2</option>
          <option value="3">Variant 3</option>
        </select>
        <br />
        <label for="base-color">Base Color</label>
        <input
          value={this.state.baseColor}
          name="base-color"
          onChange={this.onChangeBaseColor}
        />
        <br />
        <button onClick={this.generateColorScheme}>
          Color Scheme from Base Color
        </button>
        <button onClick={this.generateRandomColorScheme}>
          Random Color Scheme
        </button>
        <br />
        <button onClick={this.increaseSize}>+</button>
        <button onClick={this.normalizeSize}>=</button>
        <button onClick={this.decreaseSize}>-</button>
        <div>
          <h1>Custom</h1>
          <input
            value={this.state.primaryColor}
            onChange={this.onChangePrimaryColor}
          />
          <br />
          <input
            value={this.state.secondaryColor}
            onChange={this.onChangeSecondaryColor}
          />
          <br />
          <div style={{ margin: 8, display: 'inline-block' }}>
            <div
              style={{
                cursor: 'pointer',

                mozUserSelect: 'none',
                webkitUserSelect: 'none',
                msUserSelect: 'none'
              }}
              onClick={() =>
                this.markPair([
                  this.state.primaryColor,
                  this.state.secondaryColor
                ])
              }
            >
              <Logo
                height={this.state.size}
                primary={'#' + this.state.primaryColor}
                secondary={'#' + this.state.secondaryColor}
                {...this.state.style}
              />
            </div>
          </div>
        </div>
        <div>
          <h1>Marked</h1>
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}
          >
            {markedLogos}
          </div>
        </div>
        <div>
          <h1>Generated</h1>
          <div
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}
          >
            {logos}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
